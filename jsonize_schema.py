import os
from fs import open_fs
import yaml
import json

sdir = os.path.join(os.path.dirname(__file__), 'defslib/spec/schemas')
sfs = open_fs(sdir)

for x in sfs.walk.files(filter=['*.json-schema']):
  with sfs.open(x, "r+") as f:
    a = yaml.safe_load(f.read())
    f.seek(0)
    f.write(json.dumps(a))
    f.truncate()


