import contextlib
import glob
import hashlib
import json
import jsonschema
import os
import requests
import shutil
import string
import sys
import tempfile
import toposort
import yaml

from copy import deepcopy
from fs.osfs import OSFS
from subprocess import call, check_output

class MorphologyResolver():

  def __init__(self, directory):
    '''Initializes a MorphologyResolver, taking a path to a definitions
       repository.'''
    self._dfs = OSFS(directory)
    self._sdir = os.path.join(os.path.dirname(__file__), 'spec/schemas')
    self._sfs = OSFS(self._sdir)
    self._schemas = { 'assemblage': self._load_schema('assemblage.json-schema'),
                      'chunk': self._load_schema('chunk.json-schema') ,
                      'defaults': self._load_schema('defaults.json-schema') }
    
    self.database = {}
    self.defaults = self._load_morph('DEFAULTS')
    self.validate_defaults(self.defaults)

  def lookup(self, filename):
    '''Lookup a morphology by filename. Returns a chunk or a fully resolved
       assemblage where 'morph' fields have been replaced with the contents
       of the filename.'''
    if not filename in self.database:
      a = self.database[filename] = self.parse_morph(filename)
      self.database[filename] = a

    return self.database[filename]

  def lookup_all(self, regex='*.morph'):
    '''Lookup all morphologies. Runs lookup() on all files matching the wildcard.'''
    for x in self._dfs.walkfiles(wildcard=regex):
      a = self.lookup(x)

    return self.database

  def parse_morph(self, filename):
    '''Parse a single morphology manually using this resolver.''' 
    a = self._load_morph(filename)
    return self._resolve_morph(a)

  def validate_assemblage(self, yaml):
    '''Assemblage validation, throws if fails.'''
    for x in yaml['contents']:
      try:
        if x.get('kind') == 'assemblage':
          self.validate_assemblage(x)
        else:
          self.validate_chunk(x)
      except Exception as e:
        raise Exception("Failed to validate %s" % yaml['name']) from e
    schema = self._schemas['assemblage']
    resolver = jsonschema.RefResolver('file://%s/' % self._sdir, schema)
    jsonschema.validate(yaml, schema, resolver=resolver)

  def validate_chunk(self, yaml):
    '''Chunk validation, throws if fails.'''
    jsonschema.validate(yaml, self._schemas['chunk'])

  def validate_defaults(self, yaml):
    '''Defaults validation, throws if fails.'''
    schema = self._schemas['defaults']
    resolver = jsonschema.RefResolver('file://%s/' % self._sdir, schema)

    jsonschema.validate(yaml, self._schemas['defaults'], resolver=resolver)

  def _load_morph(self, filename):
    with self._dfs.open(filename) as f:
      return yaml.safe_load(f.read())

  def _load_schema(self, filename):
    with self._sfs.open(filename) as f:
     return yaml.safe_load(f.read())

  def _resolve_morph(self, yaml):
    if yaml.get('kind', '') == 'assemblage':
      return self._resolve_assemblage(yaml)
    else:
      return yaml

  def _resolve_assemblage(self, yaml):
    for x in yaml['contents']:
      if 'morph' in x:
        a = self.lookup(x['morph'])
        x.update(a)
        del(x['morph'])
      if 'build-system' in x:
        y = deepcopy(self.defaults['build-systems'][x['build-system']])
        for z in y:
          if z not in x:
            x[z] = y[z]
        del(x['build-system'])
    return yaml


class Actuator():

  def flatten_assemblage(self, assemblage):
    '''Flattens an assemblage, reducing it to a list of chunks.'''
    contents = assemblage['contents']
    asses = list(filter(lambda x: x.get('kind', None) == 'assemblage', contents))
    for x in asses:
      sub = self.flatten_assemblage(x)['contents']
      lens = self.lens(x, contents)
      for i in sub:
        i['build-depends'] = sorted(list(set(i.get('build-depends', []) + list(map(lambda z: z['name'], filter(lambda a: a.get('build-mode', 'staging') != 'bootstrap', lens['supports']))))))
      for y in lens['burdens']:
        y['build-depends'].remove(x['name'])
        if x.get('build-mode', 'staging') != 'bootstrap':
          y['build-depends'] = sorted(list(set(y.get('build-depends', []) + list(map(lambda t: t['name'], sub)))))
      contents = self.toposort_contents(lens['supports'] + lens['burdens'] + sub + lens['noncomps'])
    assemblage['contents'] = list(filter(lambda z: z.get('kind', None) != 'assemblage', contents))
    return assemblage

  def get_includes_iterator(self, assemblage):
    '''Iterates over an assemblage, yielding only chunks that are non-bootstrap and
       included in run-time strata.'''
    for x in assemblage['contents']:
      if x.get('kind', 'chunk') == 'assemblage':
        if x.get('include-mode','run-time') == 'run-time':
           yield from self.get_includes_iterator(x)
      elif x.get('kind', 'chunk') == 'chunk':
        if x.get('build-mode', 'staging') == 'staging':
           yield x

  def get_iterator(self, assemblage, supports=[]):
    '''Iterates over an assemblage in build-order, yielding a pair of the iterand and
       a list of sandbox supports.'''
    for x in assemblage['contents']:
      if x.get('kind', 'chunk') == 'assemblage':
        yield from self.get_iterator(x, supports + self.lens(x, assemblage['contents'])['supports'])
      else:
        yield { 'focus': x, 'supports': supports + self.lens(x, assemblage['contents'])['supports'] }
    yield { 'focus': assemblage, 'supports': supports }

  def lens(self, focus, contents):
    '''A lens for a chunk in a contents list. Returns 'supports' (if x < focus),
       'burdens' (if x > focus) and 'noncomps' (if x ~ y).'''
    def comparator(x,y):
      return x['name'] in y.get('build-depends', [])

    return { 'supports': list(filter(lambda z: comparator(z, focus), contents)),
             'burdens': list(filter(lambda z: comparator(focus, z), contents)),
             'noncomps': list(filter(lambda z: not comparator(z, focus) and not comparator(focus, z), contents)) }

  def toposort_contents(self, contents):
    '''Takes a contents list and sorts it topologically.'''
    key_depends = dict((x['name'], set(x.get('build-depends', []))) for x in contents)
    topo = list(toposort.toposort_flatten(key_depends))
    sorted(contents, key=lambda x: topo.index(x['name']))
    return contents

  def cache_enrich_assemblage(self, assemblage, supports=[], global_factors={}):
    '''Takes an assemblage and enriches it with cache keys for each component (chunks
       and assemblages).'''
    contents = self.toposort_contents(assemblage['contents'])
    for x in contents:
      if 'cache' in x:
        continue
      lens = self.lens(x, contents)
      if x.get('kind', None) == 'assemblage':
        self.cache_enrich_assemblage(x, lens['supports'] + supports, global_factors)
      else:
        factors = deepcopy(x)
        if 'include-mode' in factors:
          del(factors['include-mode'])
        if 'artifacts' in factors:
          del(factors['artifacts'])
        factors.update( {
          'supports' : supports + lens['supports'],
          'global_factors' : global_factors
        })
        x['cache'] = "%s.%s" % (x['name'], hashlib.sha256(json.dumps(factors, sort_keys=True).encode('utf-8')).hexdigest())

    factors = deepcopy(assemblage)
    factors.update( { 'supports': supports,
                      'global_factors' : global_factors })
    assemblage['cache'] = "%s.%s" % (assemblage['name'], hashlib.sha256(json.dumps(factors, sort_keys=True).encode('utf-8')).hexdigest())
